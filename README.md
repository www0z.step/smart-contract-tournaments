# About

This is an AlienESI Tournaments smart contract. Currently deployed to `c2.v2349.testnet`. The smart contract lives in the `/contract` folder.

&nbsp;

# Contract Usage

The contract has change and view methods. Those methods can be called via [near-cli], [NEAR REST server], [near-api-js].  
&nbsp;

## 1. Change methods

Change methods can change the state of the blockchain i.e. write data to the blockchain. 
Change methods can be payable, i.e. you can send some tokens when calling these methods.
Change methods are silent on success. Invoke `near_sdk::panic_str()` in case of error.

### NEAR CLI

To call a change method with `near-cli`:
```
near call <contractName> <methodName> [args]
```

Frequently used arguments:
```
--accountId  Account that will be used to sign this call [string] [required]
--deposit    Number of tokens to attach (in NEAR) to a function call [string] [default: "0"]
--args       Arguments to the contract call, in JSON format (e.g. '{"param_a": "value"}') [string] [default: null]
--gas        Max amount of gas this call can use (in gas units) [string] [default: 30000000000000]
--networkId  NEAR network ID [string] [default: "testnet"]
```

For more information use `near call --help`

### NEAR REST server

To call a change method with NEAR REST server:

```json
{
  "account_id": "myaccount.testnet",
  "private_key": "2Kh6PJ...RVKETfK",
  "contract": "mycontract.testnet",
  "method": "add_message",
  "params": { "text": "Hello World" },
  "attached_gas": "100000000000000",
  "attached_tokens": "0"
}
```
For more information see [documentation](https://docs.near.org/docs/api/rest-server/contracts#call)

&nbsp;

## 2. View methods

View methods are free and let you read data from the blockchain.

### NEAR CLI

To call a view method with `near-cli`:

```
near view <contractName> <methodName> [args]
```

Frequently used arguments:
```
--args   Arguments to the view call, in JSON format (e.g. '{"param_a": "value"}')  [string] [default: null]
```
For more information use `near view --help`

### NEAR REST server

To call a change method with NEAR REST server:

```json
{
  "contract": "mycontract.testnet",
  "method": "get_message",
  "params": { "message_id": 1 }
}
```
For more information see [documentation](https://docs.near.org/docs/api/rest-server/contracts#view)

&nbsp;

# Methods

## `create_tournament`
   
Creates a new tournament or updates an existing one and stores tournament data on the blockchain.
Attached tokens should cover 135% of a total prize pool to respect perk multipliers on payouts.
Only tournaments with status `"created"` can be updated.
```json
{
   "tournament_id": 1,
   "prize_payout_scheme": [
      {
         "player_position": 1,
         "prize_base_amount": "500000000000000000000000"
      },
      {
         "player_position": 2,
         "prize_base_amount": "300000000000000000000000"
      },
      
      ...

      {
         "player_position": 16,
         "prize_base_amount": "100000000000000000000000"
      }
   ]
}
```

- `prize_payout_scheme` must contain at least one element
- `prize_base_amount` should be specified in yoctoNEAR.
- 135% of the total prize_base_amount should be attached in NEAR tokens
- on success sets tournament's status to `"created"`  
&nbsp;

Possible responses in case of errors:
- Error: no tokens attached
- Error: Tournament status is already {started/finished}

&nbsp;

## `set_participant_list`
Commits a list of participants and makes it immutable. Intended to use after checkout phase.
Accepted arguments:

```json
{
   "tournament_id": 1,
   "participants": ["user1.testnet", "user2.testnet", ... , "user256.testnet"]
}
```  
- on success sets tournament's status to `"started"`  
&nbsp;  

Possible responses:
- "Error: Tournament status is already {started/finished}"
- "Error: Tournament is not found"  

&nbsp;


## `set_winner_list`
Commits a list of winners and makes prize payouts. Intended to be used as soon as tournament ends.
Accepted arguments:
```json
{
   "tournament_id": 1,
   "winners": [
      {
         "accounts": ["user8.testnet", "user13.testnet", "user11.testnet"],
         "place": 1,
         "prize_multiplier_perk": 1.05
      },
      {
         "accounts": ["user16.testnet", "user47.testnet", "user28.testnet"],
         "place": 2,
         "prize_multiplier_perk": 1
      },

      ...

      {
         "accounts":  ["user10.testnet", "user32.testnet", "user2.testnet"],
         "place": 16,
         "prize_multiplier_perk": 1.03
      }
   ]
}
```
- `winners` must contain at least one element
- `accounts` represents team members
- `prize_multiplier_perk` is a total team bonus (will be deprecated in near future)
- only users from the participant list are allowed
- on success sets tournament's status to `"finished"`  
&nbsp;

Possible responses:
- "Error: Tournament status is {started/finished}"
- "Error: Tournament is not found"

&nbsp;

## `get_tournament_status`
   
Gets tournament status by tournament id. It is a view method. Accepts a single argument:
```json
{"tournament_id": 1}
```

Possible responses:
- "created"
- "started"
- "finished"
- "Tournament is not found"

&nbsp;

# Common error messages

- "Error: permission denied"

&nbsp;  
&nbsp;  
&nbsp;

# Contract Development Quick Start

To run this project locally:

1. Make sure you've installed [Node.js] ≥ 12
2. Install dependencies: `npm install`
3. Deploy to the auto-created dev NEAR account with `npm run deploy`


# Deploy

Every smart contract in NEAR has its [own associated account][NEAR accounts]. When you run `npm run dev`, your smart contract gets deployed to the live NEAR TestNet with a throwaway account. When you're ready to make it permanent, here's how.


Step 0: Install near-cli (optional)
-------------------------------------

[near-cli] is a command line interface (CLI) for interacting with the NEAR blockchain. It was installed to the local `node_modules` folder when you ran `npm install`, but for best ergonomics you may want to install it globally:

    npm install --global near-cli

Or, if you'd rather use the locally-installed version, you can prefix all `near` commands with `npx`

Ensure that it's installed with `near --version` (or `npx near --version`)


Step 1: Create an account for the contract
------------------------------------------

Each account on NEAR can have at most one contract deployed to it. If you've already created an account such as `your-name.testnet`, you can deploy your contract to `near-blank-project.your-name.testnet`. Assuming you've already created an account on [NEAR Wallet], here's how to create `near-blank-project.your-name.testnet`:

1. Authorize NEAR CLI, following the commands it gives you:

      near login

2. Create a subaccount (replace `YOUR-NAME` below with your actual account name):

      near create-account near-blank-project.YOUR-NAME.testnet --masterAccount YOUR-NAME.testnet


Step 2: set contract name in code
---------------------------------

Modify the line in `src/config.js` that sets the account name of the contract. Set it to the account id you used above.

    const CONTRACT_NAME = process.env.CONTRACT_NAME || 'near-blank-project.YOUR-NAME.testnet'


Step 3: deploy!
---------------

One command:

    npm run deploy

As you can see in `package.json`, this does two things:

1. builds & deploys smart contract to NEAR TestNet
2. builds & deploys frontend code to GitHub using [gh-pages]. This will only work if the project already has a repository set up on GitHub. Feel free to modify the `deploy` script in `package.json` to deploy elsewhere.


# Troubleshooting

- On Windows, if you're seeing an error containing `EPERM` it may be related to spaces in your path. Please see [this issue](https://github.com/zkat/npx/issues/209) for more details.
- If you aren't getting `/out/main.wasm` file, check if `/out` directory exists.


  [React]: https://reactjs.org/
  [create-near-app]: https://github.com/near/create-near-app
  [Node.js]: https://nodejs.org/en/download/package-manager/
  [jest]: https://jestjs.io/
  [NEAR accounts]: https://docs.near.org/docs/concepts/account
  [NEAR Wallet]: https://wallet.testnet.near.org/
  [near-cli]: https://github.com/near/near-cli
  [near-api-js]: https://docs.near.org/docs/api/javascript-library
  [gh-pages]: https://github.com/tschaub/gh-pages
  [NEAR REST server]: https://github.com/near-examples/near-api-rest-server